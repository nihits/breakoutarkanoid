﻿using UnityEngine;
using System.Collections;

public class PowerUpBehaviour : MonoBehaviour
{
    // Use this for initialization
    void Start ()
    {
    
    }
    
    // Update is called once per frame
    void Update ()
    {
    
    }

    public virtual void DoPowerUp()
    {
        GameManager.Instance.PlaySound (AudioType.BARLONG);
    }
}
