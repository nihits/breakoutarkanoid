﻿using UnityEngine;
using System.Collections;

public class CatchPowerUpBehaviour : PowerUpBehaviour
{

    // Use this for initialization
    void Start ()
    {
    
    }
    
    // Update is called once per frame
    void Update ()
    {
    
    }

    public override void DoPowerUp()
    {
        GameManager.Instance.SetBarCatch(true);
    }
}
