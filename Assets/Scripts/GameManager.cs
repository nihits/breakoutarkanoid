﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
public enum AudioType
{
    GAMESTART1,
    GAMESTART2,
    GAMESTART3,
    BRICKMETAL,
    BRICKBREAK,
    BALLKILLMONSTER,
    BARBOUNCE,
    BARSHOOT,
    BARSTOP,
    BARLONG,
    BARSTAGECHANGE,
    BARDIE,
    GAMEOVER,
}

[System.Serializable]
public class AudioObject
{
    public AudioType Type;
    public AudioClip Clip;
}

public class GameManager : MonoBehaviour
{
    public GameObject LivesPrefab;
    public List<GameObject> AllLives;
    public static uint NumLives;
    public GameObject BallPrefab;
    public List<BallMotion> AllBalls;
    public GameObject[] BrickPrefabs;
    public List<BrickBehaviour> AllBricks;
    public GameObject[] PowerUpPrefabs;
    public BarMotion Bar;
    public GameObject Dialog;
    public Text DialogText;
    public Texture2D[] AllBackgroundTextures;
    public GameObject Background;
    public uint Score;
    public Text ScoreText;
    public static uint HighScore;
    public Text HighScoreText;
    public static bool ContinueScore;
    public static uint ScoreFromLastStage;
    public static uint StageNumber;
    public static uint HighStage;
    public Text RoundText;
    public Text HighRoundText;
    public uint SessionBricksBroken;
    static GameManager _Instance;
    public List<AudioObject> AllSounds;

    public static GameManager Instance
    {
        get
        {
            if (_Instance == null)
                _Instance = new GameManager ();
            return _Instance;
        }
    }

    // Use this for initialization
    void Start ()
    {
        _Instance = this;

        if (Background != null && AllBackgroundTextures.Length > 0)
        {
            int index = Random.Range (0, AllBackgroundTextures.Length);
            Texture2D t2d = AllBackgroundTextures [index];
            //Background.GetComponent<Renderer> ().materials [0].SetTexture ("_MainTex", t2d);
            Background.GetComponent<Renderer> ().material.mainTexture = t2d;
        }

        if (ContinueScore)
        {
            Score = ScoreFromLastStage;
        }
        else
        {
            Score = 0;
            StageNumber = 0;
            NumLives = 3;
        }

        RoundText.text = "Round " + (StageNumber + 1).ToString ();
        ScoreText.text = Score.ToString ();
        ContinueScore = false;
        ScoreFromLastStage = 0;

        int highStage = PlayerPrefs.GetInt ("HighStage");
        if (highStage > HighStage)
            HighStage = (uint)highStage;
        HighRoundText.text = "Round " + (HighStage + 1).ToString ();

        int high = PlayerPrefs.GetInt ("HighScore");
        if (high > HighScore)
            HighScore = (uint)high;

        HighScoreText.text = HighScore.ToString ();

        SetBarFireLaser(false);
        SetBarCatch(false);
        SetBarLong(false);

        SessionBricksBroken = 0;
        AllBricks = new List<BrickBehaviour> ();
        float onethird = 1f / 3.0f;

        while (AllBricks.Count == 0)
        {
            int times = 0;
            for (float y = 1.5f, z = (1.5f-onethird); y < 4.5f && z >= -2.5; y += onethird, z-=onethird)
            {
                if (times > StageNumber)
                {
                    if (Random.Range (0f, 1.0f) < 0.5f)
                        continue;
                    if (Random.Range (0f, 1.0f) < 0.5f)
                        break;
                }
                for (float x = -5.3f; x < 2.7f; x+=1)
                {
                    //if (Random.Range(0f,1.0f) < 0.5f)
                    //    continue;
                    if (Random.Range (0f, 1.0f) < 0.5f)
                    {
                        int index = Random.Range (0, BrickPrefabs.Length);
                        GameObject brick = Instantiate (BrickPrefabs [index]);
                        AllBricks.Add (brick.GetComponent<BrickBehaviour> ());
                        brick.transform.position = new Vector2 (x, y);
                    }
                    if (Random.Range (0f, 1.0f) < 0.5f)
                    {
                        int index = Random.Range (0, BrickPrefabs.Length);
                        GameObject brick = Instantiate (BrickPrefabs [index]);
                        AllBricks.Add (brick.GetComponent<BrickBehaviour> ());
                        brick.transform.position = new Vector2 (x, z);
                    }
                }
                times++;
            }
        }
        RemoveExistingAndAddLives ();
        AddBarAndBallsAndRestartMusic ();
    }

    void RemoveExistingAndAddLives ()
    {
        foreach (var life in AllLives)
        {
            Destroy (life);
        }

        uint lives = 0;
        for (float x = -5.5f; x < 2.9f && lives < NumLives-1; x += 0.85f, lives++)
        {
            GameObject life = Instantiate (LivesPrefab);
            AllLives.Add (life);
            life.transform.position = new Vector2 (x, -4.75f);
        }
    }

    void AddBarAndBallsAndRestartMusic ()
    {
        Bar.gameObject.SetActive (true);
        Bar.GetComponent<Renderer> ().enabled = true;

        AllBalls = new List<BallMotion> ();
        GameObject go = Instantiate (BallPrefab);
        BallMotion bm = go.GetComponent<BallMotion> ();
        AllBalls.Add (bm);
        go.transform.position = Bar.transform.position + new Vector3 (0, 0.275f, 0);
        go.transform.parent = Bar.transform;
        
        PlaySound (AudioType.GAMESTART1);
        ShowDialogWithText ("ROUND " + (StageNumber + 1) + "\nPRESS  SPACEBAR\n  TO  BEGIN");
    }

    public void SetPowerUpGameMode (PowerUpBehaviour pub)
    {
        if (AllBalls.Count > 0)
        {
            SetBarFireLaser(false);
            SetBarCatch(false);
            SetBarLong(false);
            pub.DoPowerUp();
            RemovePowerUp (pub);
        }
    }

    public void SetBarFireLaser(bool setFireLaser)
    {
        Bar.SetBarFireLaser(setFireLaser);
    }

    public void SetBarCatch(bool setCatch)
    {
        Bar.SetBarCatch(setCatch);
    }

    public void SetBarLong(bool setLong)
    {
        Bar.SetBarLong(setLong);
    }

    public void DoSplit()
    {
        if (AllBalls.Count < 3 && AllBalls.Count > 0)
        {
            for (int i = AllBalls.Count;i < 3;i++)
            {
                GameObject go = Instantiate (BallPrefab);
                BallMotion bm = go.GetComponent<BallMotion> ();
                AllBalls.Add (bm);
                go.transform.position = AllBalls[0].transform.position;
                Vector2 v2 = AllBalls[0].GetComponent<Rigidbody2D>().velocity;
                bm.GetComponent<Rigidbody2D>().velocity = ((i % 2) == 0) ?  new Vector2(-v2.x,v2.y) : new Vector2(v2.x,-v2.y);
            }
        }
    }
    
    public void RemovePowerUp (PowerUpBehaviour pub)
    {
        Destroy (pub.gameObject);
    }

    public void RemoveBrick (BrickBehaviour bb)
    {
        AllBricks.Remove (bb);
        SessionBricksBroken++;

        if (AllBricks.Count == 0)
        {
            GoToNextStage();
        }
        else
        {
            if (AllBalls.Count == 1 && (SessionBricksBroken % 5) == 0)
            {
                int index = Random.Range (0, PowerUpPrefabs.Length);
                GameObject powerUp = Instantiate (PowerUpPrefabs [index]);
                powerUp.transform.position = bb.transform.position;
            }
        }
        
        Destroy (bb.gameObject);
    }

    public void GoToNextStage()
    {
        foreach (var ball in AllBalls)
        {
            Rigidbody2D rb2d = ball.GetComponent<Rigidbody2D> ();
            if (rb2d != null)
                rb2d.velocity = new Vector2 ();
        }
        Score += 1000*(StageNumber+1);

        PlaySound (AudioType.BARSTAGECHANGE);
        ShowDialogWithText ("CONGRATS!\n OVER  TO  ROUND " + (StageNumber + 2) + "!!");
        Invoke ("ToNextStage", 1.5f);
    }

    void ToNextStage ()
    {
        ContinueScore = true;
        ScoreFromLastStage = Score;
        StageNumber++;

        if (StageNumber > HighStage)
        {
            HighStage = StageNumber;
            PlayerPrefs.SetInt ("HighStage", (int)HighStage);
        }

        Restart ();
    }
    
    // Update is called once per frame
    void Update ()
    {
    
    }

    public void AddScore (uint addScore)
    {
        Score += addScore;
        ScoreText.text = Score.ToString ();

        if (HighScore < Score)
        {
            HighScore = Score;
            HighScoreText.text = HighScore.ToString ();
            PlayerPrefs.SetInt ("HighScore", (int)HighScore);
        }
    }

    void OnTriggerEnter2D (Collider2D other)
    {
        if (other.gameObject.tag == "PowerUp")
        {
            RemovePowerUp (other.gameObject.GetComponent<PowerUpBehaviour> ());
        }
    }

    public void DoOneUp()
    {
        NumLives += 1;
        RemoveExistingAndAddLives();
    }

    public void DoSlowDown()
    {
        foreach(var ball in AllBalls)
        {
            ball.SlowDown();
        }
    }

    void OnCollisionEnter2D (Collision2D other)
    {
        if (other.gameObject.tag == "Ball")
        {
            BallMotion bm = other.gameObject.GetComponent<BallMotion> ();
            if (AllBalls.Contains (bm))
            {
                AllBalls.Remove (bm);
                Destroy (other.gameObject);
            }
            if (AllBalls.Count <= 0)
            {
                Bar.GetComponent<Renderer> ().enabled = false;
                Bar.GetComponent<ParticleSystem> ().Emit (100);
                PlaySound (AudioType.BARDIE);

                SetBarFireLaser(false);
                SetBarCatch(false);
                SetBarLong(false);
                NumLives--;
                if (NumLives <= 0)
                {
                    ShowDialogWithText ("FAIL!!");
                    Invoke ("Restart", 1.5f);
                }
                else
                {
                    Invoke ("StartNewLife", 1.5f);
                }
            }
        }
    }

    void StartNewLife ()
    {
        // comes here because there was a life available
        GameObject life = AllLives [AllLives.Count - 1];
        AllLives.Remove (life);
        Destroy (life);
        SetBarFireLaser(false);
        SetBarCatch(false);
        SetBarLong(false);
        AddBarAndBallsAndRestartMusic ();
    }
    
    void Restart ()
    {
        Application.LoadLevel (Application.loadedLevel);
    }

    public void ShowDialogWithText (string text)
    {
        DialogText.text = text;
        Dialog.SetActive (true);
    }
    
    public void RemoveDialog ()
    {
        DialogText.text = string.Empty;
        Dialog.SetActive (false);
    }

    public void PlaySound (AudioType type)
    {
        if (AllSounds.Count > type.GetHashCode ())
        {
            AudioObject ao = AllSounds [type.GetHashCode ()];
            AudioSource aSource = GetComponent<AudioSource> ();
            if (aSource != null && ao != null)
            {
                if (aSource.isPlaying)
                    aSource.Stop ();
                aSource.clip = ao.Clip;
                aSource.Play ();
            }
        }
    }
}
