﻿using UnityEngine;
using System.Collections;

public class BrickBehaviour : MonoBehaviour
{
    public uint Points;
    public uint KillTries;
    // Use this for initialization
    void Start()
    {
    
    }
    
    // Update is called once per frame
    void Update()
    {
    
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Ball")
        {
            if(KillTries > 0)
            {
                GameManager.Instance.PlaySound(AudioType.BRICKMETAL);
                GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
                Color ec = GetComponent<Renderer>().material.GetColor("_EmissionColor");
                ec += new Color(0.3f,0.3f,0.3f);
                GetComponent<Renderer>().material.SetColor("_EmissionColor",ec);
                //DynamicGI.SetEmissive(GetComponent<Renderer>(), Color.grey); // Color.gray
                KillTries--;
            }
            else
            {
                GameManager.Instance.PlaySound(AudioType.BRICKBREAK);
                GameManager.Instance.AddScore(Points);
                GameManager.Instance.RemoveBrick(this);
                if(Mathf.Abs(other.rigidbody.velocity.x) > Mathf.Abs(other.rigidbody.velocity.y))
                {
                    other.rigidbody.velocity = new Vector2(other.rigidbody.velocity.y,other.rigidbody.velocity.x);
                    //if (other.rigidbody.velocity.y > 3f* other.rigidbody.velocity.x)
                    //    other.rigidbody.velocity.y = 3f* other.rigidbody.velocity.x;
                }
            }
        }
    }

    void OnTriggerEnter2D (Collider2D other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            if(KillTries > 0)
            {
                GameManager.Instance.PlaySound(AudioType.BRICKMETAL);
                GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
                Color ec = GetComponent<Renderer>().material.GetColor("_EmissionColor");
                ec += new Color(0.3f,0.3f,0.3f);
                GetComponent<Renderer>().material.SetColor("_EmissionColor",ec);
                //DynamicGI.SetEmissive(GetComponent<Renderer>(), Color.grey); // Color.gray
                KillTries--;
            }
            else
            {
                GameManager.Instance.PlaySound(AudioType.BRICKBREAK);
                GameManager.Instance.AddScore(Points);
                GameManager.Instance.RemoveBrick(this);
                Destroy (other.gameObject);
            }
        }
    }

}
