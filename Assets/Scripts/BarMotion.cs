﻿using UnityEngine;
using System.Collections;

public class BarMotion : MonoBehaviour
{
    public float Speed;
    public Vector2 SixtyDegree;
    public Vector2 FortyFiveDegree;

    public Material[] AllMaterials;
    // Use this for initialization

    public bool BarFireLaser;
    public bool BarLong;
    public bool BarCatch;
    public Vector2 BarCatchDir;

    public GameObject BulletPrefab;

    void Start ()
    {
        SixtyDegree = new Vector2 (1.0f, 3.0f);
        SixtyDegree.Normalize ();
        FortyFiveDegree = new Vector2 (1.0f, 1.0f);
        FortyFiveDegree.Normalize ();
        GetComponent<Renderer>().material = AllMaterials[0];
        BarFireLaser = false;
        BarLong = false;
        BarCatch = false;
    }

    // Update is called once per frame
    void Update ()
    {
        Vector2 pos = transform.position;
        float amount = Input.GetAxis ("Horizontal") * Speed * Time.deltaTime;
        if (amount != 0f)
        {
            pos += amount * Vector2.right;
            if (BarLong)
                pos.x = Mathf.Clamp (pos.x, -4.55f, 1.95f);
            else
                pos.x = Mathf.Clamp (pos.x, -5.2f, 2.6f);
            transform.position = pos;
        }

        if (BarFireLaser && Input.GetKeyDown(KeyCode.UpArrow))
        {
            GameObject bullet1 = Instantiate (BulletPrefab);
            bullet1.transform.position = transform.position + new Vector3 (-.4f, 0.275f, 0);
            Rigidbody2D rb = bullet1.GetComponent<Rigidbody2D>();
            rb.velocity = new Vector2(0f,6.0f);
            GameObject bullet2 = Instantiate (BulletPrefab);
            bullet2.transform.position = transform.position + new Vector3 (+.4f, 0.275f, 0);
            rb = bullet2.GetComponent<Rigidbody2D>();
            rb.velocity = new Vector2(0f,6.0f);
            GameManager.Instance.PlaySound (AudioType.BARSHOOT);
        }
    }

    public void SetBarFireLaser(bool setFireLaser)
    {
        BarFireLaser = setFireLaser;

        if(setFireLaser)
            GetComponent<Renderer>().material = AllMaterials[1];
        else
            GetComponent<Renderer>().material = AllMaterials[0];
    }
    
    public void SetBarCatch(bool setCatch)
    {
        BarCatch = setCatch;
    }

    public void SetBarLong(bool setLong)
    {
        BarLong = setLong;
        if(BarLong)
            transform.localScale = new Vector3(3.0f,0.66f,transform.localScale.z);
        else
            transform.localScale = new Vector3(1.5f,0.33f,transform.localScale.z);
    }

    void OnCollisionEnter2D (Collision2D other)
    {
        if (other.gameObject.tag == "Ball")
        {
            if (other.contacts [0].point.y > transform.position.y)
            {
                Vector2 dir = SixtyDegree;
                float contact = (other.contacts [0].point.x - transform.position.x);

                if (contact < 0)
                {
                    if (contact < -0.33f)
                        dir = new Vector2 (-FortyFiveDegree.x, FortyFiveDegree.y);
                    else
                        dir = new Vector2 (-SixtyDegree.x, SixtyDegree.y);
                }
                else
                {
                    if (contact > 0.33f)
                        dir = new Vector2 (FortyFiveDegree.x, FortyFiveDegree.y);
                    //else
                    //    dir = SixtyDegree;
                }

                if(BarCatch)
                {
                    BarCatchDir = dir;
                    other.rigidbody.velocity = new Vector2();
                    other.transform.parent = this.gameObject.transform;
                    GameManager.Instance.PlaySound (AudioType.BARSTOP);
                }
                else
                {
                    other.rigidbody.velocity = dir*other.rigidbody.velocity.magnitude;
                    GameManager.Instance.PlaySound (AudioType.BARBOUNCE);
                }
            }
        }
    }

    void OnTriggerEnter2D (Collider2D other)
    {
        if (other.gameObject.tag == "PowerUp")
        {
            //GameManager.Instance.PlaySound (AudioType.BARLONG);
            PowerUpBehaviour pub = other.gameObject.GetComponent<PowerUpBehaviour> ();
            GameManager.Instance.SetPowerUpGameMode (pub);
            //GetComponent<Renderer>().material = AllMaterials[1];
        }
    }

}
