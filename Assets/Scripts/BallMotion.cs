﻿using UnityEngine;
using System.Collections;

public class BallMotion : MonoBehaviour
{
    public bool Initialized;
    public float Force;
    public static float VelocityMagnitude = 6.0f;

    void Start ()
    {
        Initialized = false;
    }
    
    // Update is called once per frame
    void Update ()
    {
        //Debug.Log (GetComponent<Rigidbody2D>().velocity.magnitude);
        if (!Initialized)
        {
            if (Input.GetKeyDown (KeyCode.Space))
            {
                GameManager.Instance.PlaySound (AudioType.GAMESTART2);
            }

            if (Input.GetKeyUp (KeyCode.Space))
            {
                GameManager.Instance.PlaySound (AudioType.GAMESTART3);
                GameManager.Instance.RemoveDialog ();
                Initialized = true;
            }
        }

        if (Input.GetKeyUp (KeyCode.Space))
        {
            Rigidbody2D rb = GetComponent<Rigidbody2D> ();

            if (rb.velocity.sqrMagnitude < 0.1f)
            {
                transform.parent = null;

                if(GameManager.Instance.Bar.BarCatch)
                {
                    rb.velocity = GameManager.Instance.Bar.BarCatchDir*VelocityMagnitude;
                }
                else
                {
                    //float xMove = Random.Range(-3.0f,3.0f);
                    Vector2 v = new Vector2 (1.0f, 3.0f);
                    v.Normalize ();
                    //rb.AddForce (v * Force);
                    rb.velocity = v*VelocityMagnitude;
                }
            }
        }
    }

    public void SlowDown()
    {
        var rb = GetComponent<Rigidbody2D>();
        Vector2 oldVelocityVector = rb.velocity;
        rb.velocity = 0.5f*oldVelocityVector;
        oldVelocityVector.Normalize();

        StartCoroutine(CatchUp(oldVelocityVector)); // need to StopCoroutine("CatchUp") before destroying this ball?
    }

    IEnumerator CatchUp(Vector2 OrigNormalized)
    {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        float mag = rb.velocity.magnitude;

        float secondsElapsed = 0.0f;
        float fullTime = 10.0f;

        // fix resetting the velocity at bar.
        //while( secondsElapsed < fullTime )
        do
        {
            secondsElapsed += Time.deltaTime;
            float t = secondsElapsed / fullTime;

            mag = Mathf.Lerp(mag, VelocityMagnitude, t);
            rb.velocity = OrigNormalized*mag;
            float delay = Random.Range(0.5f,1.0f);
            yield return new WaitForSeconds(delay);
            OrigNormalized = rb.velocity.normalized;
            mag = rb.velocity.magnitude;
        }
        while (mag < 5.9f);
        rb.velocity = rb.velocity.normalized*VelocityMagnitude;
    }
}
